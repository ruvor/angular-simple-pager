angular
    .module("testbed", ["simplePager"])
    .controller("ExampleController", ExampleController);

function ExampleController() {
    var self = this; //это нужно, чтобы обработчик клика мог изменять страницу

    this.showPrevNext = true;
    this.prev = "Назад";
    this.next = "Вперёд";
    this.maxPages = 9;
    this.totalPages = 123;
    //this.currentPage = 4; //можно и не задавать, будет отсутствовать текущая страница
    //this.currentPage = 154; //можно задать вне диапазона, тоже будет отсутствовать

    this.getItemsPage = function (page) {
        //alert("Запрошена страница номер " + page);
        //дальше можно включать грузило, асинхронно запрашивать новую страницу
        if (Math.random() < .99) {
            self.currentPage = page;
        }
        else {
            //типа, ошибка
            alert("Ошибка");
            //и не менять текущую страницу
        }
    };
}
