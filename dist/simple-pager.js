(function () {
    angular
        .module("simplePager", [])
        .directive("simplePager", directiveFactory);

    function directiveFactory() {
        return {
            scope: {
                showPrevNext: "<",
                prevText: "@prev",
                nextText: "@next",
                maxPagesShown: "<maxPages",
                totalPages: "<",
                currentPage: "<",
                selectPage: "<triggerFn"
            },

            restrict: 'A',

            template: "\
                <ul class='pagination' ng-if='totalPages>1'>\
                    <li ng-if='showPrevNext&&currentPage>1'><a href='javascript:void(0)' ng-click='selectPage(currentPage-1)'>{{prevText}}</a></li>\
                    <li ng-class='{active:currentPage==1}'><span ng-if='currentPage==1'>1</span><a href='javascript:void(0)' ng-if='currentPage!=1' ng-click='selectPage(1)'>1</a></li>\
                    <li class='ellipsis' ng-if='showStartEllipsis()'><span>…</span></li>\
                    <li ng-repeat='page in makeRange()' ng-class='{active:currentPage==page}'><span ng-if='currentPage==page'>{{page}}</span><a href='javascript:void(0)' ng-if='currentPage!=page' ng-click='selectPage(page)'>{{page}}</a> </li>\
                    <li class='ellipsis' ng-if='showFinishEllipsis()'><span>…</span></li>\
                    <li ng-class='{active:currentPage==totalPages}'><span ng-if='currentPage==totalPages'>{{totalPages}}</span><a href='javascript:void(0)' ng-if='currentPage!=totalPages' ng-click='selectPage(totalPages)'>{{totalPages}}</a></li>\
                    <li ng-if='showPrevNext&&currentPage<totalPages'><a href='javascript:void(0)' ng-click='selectPage(currentPage+1)'>{{nextText}}</a></li>\
                </ul>",

            link: function (scope, element, attrs) {
                scope.makeRange = function () {
                    var range = [];
                    if (scope.totalPages < 3) return range;
                    var rangeStart = (scope.currentPage - (scope.maxPagesShown - 2) / 2 + 1) ^ 0;
                    if (rangeStart < 2) rangeStart = 2;
                    var rangeFinish = (rangeStart + (scope.maxPagesShown - 2) - 1) ^ 0;
                    if (rangeFinish > scope.totalPages - 1) {
                        rangeFinish = scope.totalPages - 1;
                        rangeStart = (rangeFinish - (scope.maxPagesShown - 2) + 1) ^ 0;
                        if (rangeStart < 2) rangeStart = 2;
                    }
                    for (var i = rangeStart; i <= rangeFinish; i++) {
                        range.push(i);
                    }
                    return range;
                };

                scope.showStartEllipsis = function () {
                    var range = scope.makeRange();
                    return range[0] > 2;
                };

                scope.showFinishEllipsis = function () {
                    var range = scope.makeRange();
                    return range[range.length - 1] < scope.totalPages - 1;
                };
            }
        }
    }
})();
